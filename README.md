AWS Securitygroups
=========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

AWS Security groups
=========

Create Cloudformation stack for default securitygroups.
AWS resources that will be created are:
 * CustomerOfficeSG
 * CustomerVPNSG
 * EC2Nodes
 * AdminOfficeSG
 * AdminVPNSG

These securitygroups are referenced in several other stacks.
The concept behind them is that you add your custom security rules in a custom securitygroup-ingress role that you need to create yourself (TODO: we should have a template for this).
That role could for example insert "allow tcp/22 to 123.123.123.0/24" as securitygroup ingress rule on AdminOfficeSG

 * CustomerOfficeSG - For public (i.e. internet facing) rules to/from your customer's office.
 * CustomerVPNSG    - For private (through VPN) rules to/from your customer's office.
 * EC2Nodes         - Traffic between EC2 nodes and bastion host
 * AdminOfficeSG    - Public, to your own office
 * AdminVPNSG       - Private, your office

Requirements
------------
Ansible version 2.5.4 or higher
Python 2.7.x

Required python modules:
* boto
* boto3
* awscli

Role Variables
--------------
### _Role specific_
The following defaults are set:
```yaml
aws_securitygroups:
  debug                 : False

```
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
tag_prefix      : <'mcf' by default, will be used in various tags as prefix>
```
Dependencies
------------
 * aws-vpc

Example Playbooks
----------------

```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    tag_prefix      : "mcf"
    aws_securitygroups:
      debug         : True

  roles:
    - aws-vpc
    - aws-securitygroups

```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
